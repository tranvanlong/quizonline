### QuizOnline
##  Mục đích:
Hệ thống cung cấp môi trường để thi trực tuyến ngay trên internet thông qua máy vi tính. Người dùng (User) có thể tạo ra các khóa học và tạo ra bài quiz. User có thể mời các user khác join vào khóa học để làm bài quiz online. Người dùng dễ dàng quản lí được điểm số của mình sau các lần thi. Vinh danh những user đạt top 10 user cao điểm nhất trong khóa học.
##  In Scope:
# Hệ thống có chức năng ủy quyền người dùng :
 1. Đối với người dùng thông thường(tư cách học viên):
- Đăng kí
- Đăng nhập
- Reset mật khẩu
- Update thông tin cá nhân
- Xem dashboard dành cho học viên
- Tham gia vào các lớp học khác nhau
- Tham gia thi trắc nghiệm trực tuyến thông qua máy tính và internet
- Theo dõi điểm số sau mỗi lần thi và bảng vinh danh top 10 thí sinh cao điểm nhất (nếu đạt được)
- Đăng xuất
2. Đối với người dùng là quản lí lớp(người tạo ra lớp):
- Đăng kí
- Đăng nhập
- Reset mật khẩu
- Update thông tin cá nhân
- Xem dashboard dành cho quản lí lớp
- Tạo lớp
- Thêm người thi
- Thêm ngân hàng đề
- Tạo quiz thi
- Thực hiện thi cho học viên( Cung cấp mã code của bài quiz để học viên tham gia  vào)
- Theo dõi điểm số của tất cả các học viên sau mỗi bài quiz
- Đăng xuất
3. Đối với người dùng là admin:
- Đăng nhập
- Reset mật khẩu
- Update thông tin cá nhân
- Xem dashboard dành cho Admin
- Quản lí người dùng
- Quản lí nội dung đề thi
- Theo dõi quản lí lớp
- Theo dõi việc thi của học viên
- Đăng xuất
## Demo
- https://bit.ly/demoquizonline

